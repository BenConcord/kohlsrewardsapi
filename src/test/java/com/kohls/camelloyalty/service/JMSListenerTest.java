package com.kohls.camelloyalty.service;

import org.apache.camel.test.junit4.TestSupport;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.InputStream;
import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.kohls.rewardsapi.config.Application;

import org.apache.camel.producer;

@Test
public class JMSListenerTest {

	Application application = new Application();
	
	public void init() {
		InputStream is = getClass().getResourceAsStream("jmsListenerContext.xml");
		RoutesDefinition routes = context.loadRoutesDefintion(is);
		context.addRouteDefinitions(routes.getRoutes());
	}	
	
	
	
	public void testConnection() {
		Application application = new Application("application-TEST.properties");
		this.init();
		assertNotNull(application.jmsqueue);
	}
	

}
