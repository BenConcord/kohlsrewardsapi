package com.kohls.rewardsapi.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Application {
	

	public static  String jmsqueue = null;
	public static String userauth = null;
	public static  String pswd = null;
	public static  String tibrewards = null;
	
	
		public String filename = null;
		public Application() {
			filename = "application.properties";
			envprops();
			
		}
		public Application(String filename) {
			this.filename = filename;
			envprops();
		}

		public static void main(String[] args) {
			SpringApplication.run(Application.class, args);
		}
		
	
	public static void envprops () {
		

		Properties prop = new Properties();
		InputStream input = null;
		
	try {
		input = Application.class.getClassoader().getResourceStream(this.filename);
		if(input==null) {
			log("file not found");
			return;
		}
		prop.load(input);


	jmsqueue = prop.getProperty("jmsport");
	userauth = prop.getProperty("username");
		pswd = prop.getProperty("password");
	tibrewards = prop.getProperty("soaprewards");
		
	} catch(IOException ex) {
		ex.printStackTrace();
		
	}
	

	
	}
	
}
