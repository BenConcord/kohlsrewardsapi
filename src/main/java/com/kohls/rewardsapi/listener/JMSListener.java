package com.kohls.rewardsapi.listener;

import java.io.InputStream;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.kohls.rewardsapi.config.Application;

import org.apache.camel.producer;


public class JMSListener {

	Application application = new Application();

	
	public JMSListener() {
		Application application = new Application();
		this.jmsqueue = queue;
	}
	public void init() {
		InputStream is = getClass().getResourceAsStream("jmsListenerContext.xml");
		RoutesDefinition routes = context.loadRoutesDefintion(is);
		context.addRouteDefinitions(routes.getRoutes());
	}
	
	
}
